﻿import pandas as pd
import numpy as np
import tensorflow as tf


sess = tf.InteractiveSession() 
train_df = pd.read_csv('train.csv')
test_df = pd.read_csv('test.csv')

#print(train_df)
#print(test_df)
train_df.info()
#print('_'*40)
#test_df.info()

train_df['Title']= train_df['Name'].copy()
test_df['Title']= test_df['Name'].copy()
#for dataset in train_df['Title']:
#	print(dataset)
train_df['Title'] = train_df['Title'].str.extract(' ([A-Za-z]+)\.', expand=False)
test_df['Title'] = test_df['Title'].str.extract(' ([A-Za-z]+)\.', expand=False)
#print(train_df['Title'])
train_df['Title'].fillna(0)    
test_df['Title'].fillna(0)    
#print(train_df['Title'])
train_df['Age'].fillna((train_df['Age'].mean()),inplace=True)
train_df.isnull().any()
test_df['Age'].fillna((test_df['Age'].mean()),inplace=True)
train_df['Fare'].fillna((train_df['Fare'].mean()),inplace=True)
test_df['Fare'].fillna((test_df['Fare'].mean()),inplace=True)
train_df['Cabin'].fillna('C',inplace=True)
train_df['Embarked'].fillna('C',inplace=True)
test_df['Cabin'].fillna('C',inplace=True)
passengers = train_df['PassengerId']
train_df = train_df.drop(['PassengerId'],axis=1)
test_df = test_df.drop(['PassengerId'],axis=1)
print(test_df.isnull().any())
print(train_df.isnull().any())
#train_df.loc[:, train_df.isnull().any()]
#define data building blocks
#COLUMNS = ["PassengerId",
COLUMNS = [ "Survived","Pclass","Name","Sex","Age",
        "SibSp","Parch","Ticket","Fare","Cabin","Embarked", "Title"]
LABEL_COLUMN = "label"
CONTINUOUS_COLUMNS = ["Age","Ticket","Fare"]
CATEGORICAL_COLUMNS = [ "Sex", "Pclass","Embarked","SibSp","Cabin","Parch"]

sex = tf.contrib.layers.sparse_column_with_keys(column_name="Sex", keys=["female","male"])
pclass = tf.contrib.layers.sparse_column_with_keys(column_name="Pclass", keys = [1,2,3])
embark = tf.contrib.layers.sparse_column_with_keys(column_name="Embarked", keys = ["C","Q","S"])
sib = tf.contrib.layers.sparse_column_with_hash_bucket("SibSp", hash_bucket_size=1000)
parch = tf.contrib.layers.sparse_column_with_hash_bucket("Parch", hash_bucket_size=1000)
cabin = tf.contrib.layers.sparse_column_with_hash_bucket("Cabin", hash_bucket_size=1000)
name = tf.contrib.layers.sparse_column_with_hash_bucket("Name", hash_bucket_size=1000) 
#continuous 
age = tf.contrib.layers.sparse_column_with_hash_bucket("Age",hash_bucket_size=1000)
#age_buckets = tf.contrib.layers.bucketized_column(age, 
#                                                  boundaries=[
#                                                      18, 25, 30,
#                                                      35, 40, 45, 
#                                                      50, 55, 60,
#                                                      65])
ticket = tf.contrib.layers.sparse_column_with_hash_bucket("Ticket", hash_bucket_size=1000) 
fare = tf.contrib.layers.real_valued_column("Fare")
#print(train_df['Age'])
#train_df['PassengerId'].dropna()
def input_fn(df):
  # Creates a dictionary mapping from each continuous feature column name (k) to
  # the values of that column stored in a constant Tensor.
  continuous_cols = {k: tf.constant(df[k].values)
                     for k in CONTINUOUS_COLUMNS}
  # Creates a dictionary mapping from each categorical feature column name (k)
  # to the values of that column stored in a tf.SparseTensor.
  categorical_cols = {k: tf.SparseTensor(
      indices=[[i, 0] for i in range(df[k].size)],
      values=df[k].values,
      dense_shape=[df[k].size, 1])
                      for k in CATEGORICAL_COLUMNS}
  # Merges the two dictionaries into one.
  feature_cols = dict(continuous_cols.items() | categorical_cols.items())
  # Converts the label column into a constant Tensor.
  print(df.dtypes.index.values)
  #label = tf.constant(df[LABEL_COLUMN])
  label = tf.placeholder(tf.int32, shape=(df))
  # Returns the feature columns and the label.
  return feature_cols, label

def train_input_fn():
  return input_fn(train_df)

def eval_input_fn():
  return input_fn(test_df)

wide_columns = [sex, pclass, cabin, 
#age_buckets,
age,
sib,
#            tf.contrib.layers.crossed_column([sex,age_buckets],
            tf.contrib.layers.crossed_column([sex,age],
                                         hash_bucket_size=int(1e4)),
            tf.contrib.layers.crossed_column([sex,pclass],
                                         hash_bucket_size=int(1e4)),
            tf.contrib.layers.crossed_column([sex,pclass,sib],
                                         hash_bucket_size=int(1e6))]
deep_columns = [
        tf.contrib.layers.embedding_column(pclass, dimension=8),        
        tf.contrib.layers.embedding_column(cabin, dimension=8),        
        tf.contrib.layers.embedding_column(embark, dimension=8),        
        #tf.contrib.layers.embedding_column(fare, dimension=8),        
        #tf.contrib.layers.embedding_column(ticket, dimension=8),
        sex,
        age,
        sib,
        parch,
        fare,ticket]

import tempfile
model_dir = tempfile.mkdtemp()
m = tf.contrib.learn.DNNLinearCombinedClassifier(
    model_dir=model_dir,
    linear_feature_columns=wide_columns,
    dnn_feature_columns=deep_columns,
    dnn_hidden_units=[100, 50])


m.fit(input_fn=train_input_fn,steps=200)
results = m.evaluate(input_fn=eval_input_fn, steps=1)

#sess.run(m, feed_dict={train_df})


