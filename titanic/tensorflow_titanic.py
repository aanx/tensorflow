import tensorflow as tf
import pandas as pd
import argparse
import sys
import tempfile

#define data building blocks
COLUMNS = ["PassengerId","Survived","Pclass","Name","Sex","Age",
        "SibSp","Parch","Ticket","Fare","Cabin","Embarked"]
LABEL_COLUMN = "label"
CONTINUOUS_COLUMNS = ["Age","Ticket","Fare"]
CATEGORICAL_COLUMNS = [ "Sex", "Pclass","Embarked","SibSp","Cabin","Parch"]


# categorical data

sex = tf.contrib.layers.sparse_column_with_keys(column_name="Sex", keys=["female","male"])
pclass = tf.contrib.layers.sparse_column_with_keys(column_name="Pclass", keys = [1,2,3])
embark = tf.contrib.layers.sparse_column_with_keys(column_name="Embarked", keys = ["C","Q","S"])
sib = tf.contrib.layers.sparse_column_with_hash_bucket("SibSp", hash_bucket_size=1000)
parch = tf.contrib.layers.sparse_column_with_hash_bucket("Parch", hash_bucket_size=1000)
cabin = tf.contrib.layers.sparse_column_with_hash_bucket("Cabin", hash_bucket_size=1000)

#continuous 
age = tf.contrib.layers.real_valued_column("Age")
age_buckets = tf.contrib.layers.bucketized_column(age, 
                                                  boundaries=[
                                                      18, 25, 30,
                                                      35, 40, 45, 
                                                      50, 55, 60,
                                                      65])
ticket = tf.contrib.layers.real_valued_column("Ticket")
fare = tf.contrib.layers.real_valued_column("Fare")



#set up wide and deep columns
def build_estimator(model_dir, model_type):
    wide_columns = [sex, pclass, cabin, age_buckets,sib,
            tf.contrib.layers.crossed_column([sex,age_buckets],
                                         hash_bucket_size=int(1e4)),
            tf.contrib.layers.crossed_column([sex,pclass],
                                         hash_bucket_size=int(1e4)),
            tf.contrib.layers.crossed_column([sex,pclass,sib],
                                         hash_bucket_size=int(1e6))]
    deep_columns = [
        tf.contrib.layers.embedding_column(pclass, dimension=8),        
        tf.contrib.layers.embedding_column(cabin, dimension=8),        
        tf.contrib.layers.embedding_column(embark, dimension=8),        
        #tf.contrib.layers.embedding_column(fare, dimension=8),        
        #tf.contrib.layers.embedding_column(ticket, dimension=8),
        sex,
        age,
        sib,
        parch,
        fare,ticket]

# set up models

    if model_type == "wide":
        m = tf.contrib.learn.LinearClassifier(model_dir=model_dir,
                                          feature_columns=wide_columns)
    elif model_type == "deep":
        m = tf.contrib.learn.DNNClassifier(model_dir=model_dir,
                                       feature_columns=deep_columns,
                                       hidden_units=[100, 50])
    else:
        m = tf.contrib.learn.DNNLinearCombinedClassifier(
            model_dir=model_dir,
            linear_feature_columns=wide_columns,
            dnn_feature_columns=deep_columns,
            dnn_hidden_units=[100, 50],
            fix_global_step_increment_bug=True)
    return m

def input_fn(df):
    """Input builder function."""
    # Creates a dictionary mapping from each continuous feature column name (k) to
    # the values of that column stored in a constant Tensor.
    continuous_cols = {k: tf.constant(df[k].values) for k in CONTINUOUS_COLUMNS}
    # Creates a dictionary mapping from each categorical feature column name (k)
    # to the values of that column stored in a tf.SparseTensor.
    categorical_cols = {
       k: tf.SparseTensor(
              indices=[[i, 0] for i in range(df[k].size)],
              values=df[k].values,
            dense_shape=[df[k].size, 1])
       for k in CATEGORICAL_COLUMNS}
    # Merges the two dictionaries into one.
    feature_cols = dict(continuous_cols)
    feature_cols.update(categorical_cols)
    # Converts the label column into a constant Tensor.
    label = tf.constant(df[LABEL_COLUMN].values)
    # Returns the feature columns and the label.
    return feature_cols, label

def train_and_eval(model_dir, model_type, train_steps, train_data, test_data):
    """Train and evaluate the model."""
    #df_train = pd.read_csv("train.csv")
    #df_test = pd.read_csv("test.csv")
    df_train = pd.read_csv(
      tf.gfile.Open("train.csv"),
      names=COLUMNS,
      skipinitialspace=True,
      skiprows=1,
      engine="python")
    df_test = pd.read_csv(
      tf.gfile.Open("test.csv"),
      names=COLUMNS,
      skipinitialspace=True,
      skiprows=1,
      engine="python")
    # remove NaN elements
    df_train = df_train.dropna(how='any', axis=0)
    df_test = df_test.dropna(how='any', axis=0)
    df_train[LABEL_COLUMN] = (
          df_train["Survived"])
    #df_test[LABEL_COLUMN] = (
      #df_test["Survived"].apply(lambda x: "1" in x)).astype(int)
      #.apply(lambda x: ">50K" in x)).astype(int)

    model_dir = tempfile.mkdtemp() if not model_dir else model_dir
    print("model directory = %s" % model_dir)

    m = build_estimator(model_dir, model_type)
    a = input_fn(df_train)
    print(a[0]) 
    m.fit(input_fn=df_train, steps=train_steps)
    results = m.predict_classes(input_fn=df_test)
    for key in sorted(results):
        print("%s: %s" % (key, results[key]))


FLAGS = None


def main(_):
    train_and_eval("", "wide_n_deep", "200",
                 "", "")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.register("type", "bool", lambda v: v.lower() == "true")
    parser.add_argument(
          "--model_dir",
      type=str,
      default="",
      help="Base directory for output models."
    )
    parser.add_argument(
      "--model_type",
      type=str,
      default="wide_n_deep",
      help="Valid model types: {'wide', 'deep', 'wide_n_deep'}."
    )
    parser.add_argument(
      "--train_steps",
      type=int,
      default=200,
      help="Number of training steps."
    )
    parser.add_argument(
      "--train_data",
      type=str,
      default="",
      help="Path to the training data."
    )
    parser.add_argument(
      "--test_data",
      type=str,
      default="",
      help="Path to the test data."
    )
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
