import numpy as np
import pandas as pd
import tensorflow as tf
import seaborn as sns
from pandas.tools.plotting import andrews_curves
from pandas.tools.plotting import parallel_coordinates
import matplotlib.pyplot as plt
from tensorflow.python import debug as tf_debug


#set true to turn on debug
if False:
	hooks = [tf_debug.LocalCLIDebugHook()]
	def has_inf_or_nan(datum, tensor):
		  return np.any(np.isnan(tensor)) or np.any(np.isinf(tensor))
else:
	hooks = 0

def convert_categorical_column(data):
	tmp = pd.Series(data).astype('category')
	print('_'*40 + '\n' )
	print(tmp.cat.categories)
	#print(tmp.cat.codes)
	print('_'*40 + '\n' )
	tmp = tmp.cat.codes
	tmp = tmp.astype(np.int64)
	return tmp



tf.logging.set_verbosity(tf.logging.INFO)

df = pd.read_csv("M2017_train.csv")
dt = pd.read_csv("M2017_test_students.csv")

#visualizing the data
#print(df.head())
#print(df['GRADE'].describe())

#sns.boxplot(data=df,x='PARTICIPATION',y='SCORE',hue='participation')
#sns.boxplot(data=df,x='GRADE',y='SCORE',hue='GRADE')

#sns.distplot(df['GRADE'])
#sns.pairplot(df.drop(['STUDENTID'],axis=1))

#

# must do data manipulation

print(df.dtypes)
df['LEAVES_EARLY'] = convert_categorical_column(df['LEAVES_EARLY'])
df['GRADE'] = convert_categorical_column(df['GRADE'])
df['ASKS_QUESTIONS'] = convert_categorical_column(df['ASKS_QUESTIONS'])

dt['LEAVES_EARLY'] = convert_categorical_column(dt['LEAVES_EARLY'])
dt['ASKS_QUESTIONS'] = convert_categorical_column(dt['ASKS_QUESTIONS'])
#df['SxP'] = df['SCORE']*.9 + df['PARTICIPATION']*10
#dt['SxP'] = dt['SCORE']*.9 + dt['PARTICIPATION']*10
# parallel_coordinates(df.drop(["STUDENTID"], axis=1), "GRADE")
# plt.figure()
# sns.heatmap(df.corr(),vmax=.8,square=False)
# plt.show()
# input()
df = df.drop(['STUDENTID'],axis=1)
t_sid = dt['STUDENTID']
dt = dt.drop(['STUDENTID'],axis=1)
df = df.sample(frac=1)
print(df.describe())
input()
train = df.sample(frac=.8,random_state=20)
ev = df.drop(train.index)

data = [x for x in train.columns[:]if x not in ['GRADE', 'SCORE']]
clf = train.columns[1]

print(data)
input()

print(dt.dtypes)
features = []

for x in data:
 	features += [tf.contrib.layers.real_valued_column(x)]

score = tf.contrib.layers.real_valued_column('SCORE')
score_bucket = tf.contrib.layers.bucketized_column(score,
													boundaries = [20,40,60,80])
features+=[score]
features+= [score_bucket]



#features  = [tf.contrib.layers.real_valued_column("", dimension=4)]
def my_input_test():
	cols = {k: tf.constant(dt[k].values)
        	for k in dt.dtypes.index.values}
    	

#	label = tf.placeholder(tf.int32,shape=len(dt[1].values))
	return cols


def input_eval():
	return input_fn(ev)

def input_fn(d=train):
	cols = {k: tf.constant(d[k].values)
        	for k in d.dtypes.index.values}
    	

	label = tf.constant(d[clf].values)
	return cols, label
lin = tf.contrib.learn.LinearClassifier(feature_columns=features,
											n_classes=5,
											model_dir="model/lin/")
dnn = tf.contrib.learn.DNNClassifier(feature_columns=features,
                                            hidden_units=[10, 20, 10],
                                            n_classes=5,
                                            model_dir="model/dnn/")

sess = tf.Session()
sess.run(tf.global_variables_initializer())

lin.fit(input_fn=input_fn,steps=1000)
prd = lin.predict_classes(input_fn=my_input_test)
print(prd)

acc=lin.evaluate(input_fn=input_eval,steps=1)['accuracy']
print("\n ACCURACY: {0:f}\n".format(acc))

input()
dnn.fit(input_fn=input_fn,steps=1000)
acc=dnn.evaluate(input_fn=input_eval,steps=1)['accuracy']
print("\n ACCURACY: {0:f}\n".format(acc))
prd = dnn.predict_classes(input_fn=my_input_test)


#print (list(prd))
prd = list(prd)
print(prd)
prd = np.reshape(prd,861,1)
print(prd)

out = pd.DataFrame() 
out['STUDENTID'] = t_sid
out['GRADE'] = prd
out['GRADE'] = out['GRADE'].astype(int)
map_dict = {0: "A",1:"B",2:"C",3:"D",4:"F"}
out['GRADE'] = out['GRADE'].map(map_dict)
out['STUDENTID'] = out['STUDENTID'].astype(int)
print(out['GRADE'])
print(out.dtypes)
print(out.dtypes.index)
out.sort_values(['STUDENTID', 'GRADE'],inplace=True,ascending=True)
out.to_csv('output.csv',index=False,header=True)




